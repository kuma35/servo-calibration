// Stack class body
// 2013/07/04 by kuma35

#include <stdlib.h>
#include "Stack.h"


Stack::Stack() {
  _stack_size = _stack_size_default;
  _stack_pointer = _stack_empty;
}


void Stack::clear(void) {
  _stack_pointer = _stack_empty;
}


int Stack::popable(void) {
  return (_stack_pointer > _stack_empty);
}


int Stack::pushable(void) {
  return (_stack_pointer < _stack_size);
}


void Stack::push(int value) {
  if (this->pushable()) {
    ++(_stack_pointer);
    _stack_buffer[_stack_pointer] = value;
  }
}

int Stack::pop(void) {
  int value;
  if (Stack::popable()) {
    value = _stack_buffer[_stack_pointer];
    (_stack_pointer)--;
  }
  return value;
}


void Stack::dump(HardwareSerial &serial) {
  serial.print("data-stack:");
  if (this->popable()) {
    for (int i=0;i<=_stack_pointer;i++) {
      serial.print(_stack_buffer[i]);
      serial.print(",");
    }
    serial.println("");
  } else {
    serial.println("stack empty.");
  }
}
