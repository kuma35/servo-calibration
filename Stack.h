// Stack class header
// 2013/07/04 by kuma35

#ifndef _Stack_h_
#define _Stack_h_
#include <arduino.h>
#include <HardwareSerial.h>

#define STACK_SIZE_DEFAULT_INIT 16

class Stack {
 private:
  static const int _stack_size_default = STACK_SIZE_DEFAULT_INIT;
  static const int _stack_empty = -1;
  int _stack_size;
  int _stack_buffer[STACK_SIZE_DEFAULT_INIT];
  int _stack_pointer;
 public:
  Stack();
  int popable(void);
  int pop(void);
  int pushable(void);
  void push(int value);
  void clear(void);
  void dump(HardwareSerial &serial);
};

#endif
