// ServoCalibration
// 
// 

// syntax
// 1. separate by space or newline(\n)
// 2. [0-9]+ by parseInt()
// 3. other word is command ([a-z][0-9a-z]+)

#define SERVO_PAN_PIN 9
#define SERVO_TILT_PIN 10

#include <Servo.h> 
#include "Stack.h"

Servo pan;
Servo tilt;
Stack DataStack;


void setup() 
{
  Serial.begin(9600);
  pan.attach(SERVO_PAN_PIN);
  tilt.attach(SERVO_TILT_PIN);
  DataStack = Stack();
} 
 
 
void loop()
{
  int c;
  if (Serial.available() > 0) {
    c = Serial.peek();
    if ('0' <= c && c <= '9' || c == '+' || c == '-') {
      if (!DataStack.pushable()) {
	Serial.println('data stack overflow!!');
	Serial.flush();
	return;
      }
      int value = Serial.parseInt();
      //Serial.print("pushed value:");
      //Serial.println(value);
      DataStack.push(value);
    } else if (c == '\x09' || c == ' ') {
      // eat and skip
      c = Serial.read();
      return;
    } else {
      // non digit and non spaces
      String word;
      word = String();
      word = Serial.readStringUntil(' ');
      if (word.length() > 0) {
	if (word.compareTo("clear") == 0) {
	  DataStack.clear();
	  DataStack.dump(Serial);
	} else if (word.compareTo("drop") == 0) {
	  if (DataStack.popable()) {
	    int dummy;
	    dummy = DataStack.pop();
	  }
	} else if (word.compareTo("dump") == 0) {
	  DataStack.dump(Serial);
	} else if (word.compareTo(".") == 0) {
	  if (DataStack.popable()) {
	    int value;
	    value = DataStack.pop();
	    Serial.print(value);
	  } else {
	    Serial.println("stack underflow!!");
	  }
	} else if (word.compareTo("cr") == 0 ) {
	  Serial.println("");
	} else if (word.compareTo("pan") == 0 ) {
	  if (DataStack.popable()) {
	    pan.writeMicroseconds(DataStack.pop());
	  } else {
	    Serial.println("stack underflow!!");
	  }
	} else if (word.compareTo("tilt") == 0 ) {
	  if (DataStack.popable()) {
	    tilt.writeMicroseconds(DataStack.pop());
	  } else {
	    Serial.println("stack underflow!!");
	  }
	} else if (word.compareTo("delay") == 0 ) {
	  if (DataStack.popable()) {
	    delay(DataStack.pop());
	  } else {
	    Serial.println("stack underflow!!");
	  }
	} else {
	  Serial.print("unknown word:");
	  Serial.println(word);
	}
      }
      return;
    }
  }
} 
